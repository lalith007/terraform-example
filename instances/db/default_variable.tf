variable "aws_key_name" {}
variable "user_name" {}
variable "aws_region" {}
variable "ami" {}

variable "vpc_id" {}
variable "private_subnet_id" {}
variable "public_subnet_cidr" {}


variable "instance_size" {
    description = "Instance size of bastion"
    default = "t2.micro"
}

variable "env" {
    description = "Env Description"
    default = "test"
}