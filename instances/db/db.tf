provider "aws" {
    region = "${var.aws_region}"
}

resource "aws_security_group" "db" {
    name = "db"
    description = "Allow traffic to pass from the publuc subnet to private subnet"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["${var.public_subnet_cidr}"]
    }
    ingress {
        from_port = 443
        to_port = 443
        protocol = "tcp"
        cidr_blocks = ["${var.public_subnet_cidr}"]
    }
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["${var.public_subnet_cidr}"]
    }
    ingress {
        from_port = 9200
        to_port = 9200
        protocol = "tcp"
        cidr_blocks = ["${var.public_subnet_cidr}"]
    }
    ingress {
        from_port = -1
        to_port = -1
        protocol = "icmp"
        cidr_blocks = ["${var.public_subnet_cidr}"]
    }
    egress {
        from_port   = 0
        to_port     = 0
        protocol    = "-1"
        cidr_blocks = ["0.0.0.0/0"]
    }

    vpc_id = "${var.vpc_id}"

    tags {
        Name        = "DB"
        Enviornment = "${var.env}"
		CreatedBy   = "${var.user_name}"
    }
}


resource "aws_instance" "db" {
    ami                         = "${var.ami}"
    instance_type               = "${var.instance_size}"
    key_name                    = "${var.aws_key_name}"
    vpc_security_group_ids      = ["${aws_security_group.db.id}"]
    subnet_id                   = "${var.private_subnet_id}"
    associate_public_ip_address = false
    tags {
        Name        = "DB Instance"
		Enviornment = "${var.env}"
		CreatedBy   = "${var.user_name}"
    }    
}

output "db_ip" {
    value = "${aws_instance.db.private_ip}"
}